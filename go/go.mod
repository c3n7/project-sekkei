module github.com/c3n7/project-sekkei/go

go 1.14

require (
	github.com/go-flutter-desktop/go-flutter v0.41.2
	github.com/pkg/errors v0.9.1
)
